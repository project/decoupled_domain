<?php

namespace Drupal\decoupled_domain\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\decoupled_domain\Plugin\DecoupledDomainConfigInterface;

/**
 * Provides an interface for defining Decoupled Domain entities.
 */
interface DomainInterface extends ConfigEntityInterface {

  /**
   * Returns the domain name.
   *
   * @return string
   *   The name of the decoupled domain.
   */
  public function getName();

  /**
   * Sets the name of the domain.
   *
   * @param string $name
   *   The name of the domain.
   *
   * @return $this
   *   The class instance this method is called on.
   */
  public function setName($name);

  /**
   * Returns the domain list.
   *
   * @return array
   *   The domain list.
   */
  public function getDomains();

  /**
   * Returns the default domain.
   *
   * @return array
   *   The default domain.
   */
  public function getDefaultDomain();

  /**
   * Returns a specific domain config.
   *
   * @param string $config
   *   The domain config ID.
   *
   * @return \Drupal\decoupled_domain\Plugin\DecoupledDomainConfigInterface
   *   The domain config object.
   */
  public function getConfig($config);

  /**
   * Returns the domain configurations.
   *
   * @return \Drupal\decoupled_domain\DomainConfigPluginCollection
   *   The domain config plugin collection.
   */
  public function getConfigs();

  /**
   * Saves a domain config for this domain.
   *
   * @param array $configuration
   *   An array of domain config configuration.
   *
   * @return string
   *   The domain config ID.
   */
  public function addConfig(array $configuration);

  /**
   * Deletes a domain config from this domain.
   *
   * @param \Drupal\decoupled_domain\Plugin\DecoupledDomainConfigInterface $config
   *   The domain config object.
   *
   * @return $this
   *   The decoupled domain entity.
   */
  public function deleteConfig(DecoupledDomainConfigInterface $config);

}
