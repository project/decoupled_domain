<?php

namespace Drupal\decoupled_domain\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Decoupled Domain Field settings entity.
 *
 * @ConfigEntityType(
 *   id = "decoupled_domain_field_settings",
 *   label = @Translation("Decoupled Domain field settings"),
 *   handlers = {},
 *   config_prefix = "field",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "settings" = "settings"
 *   },
 *   config_export = {
 *     "id",
 *     "entity_type_id",
 *     "entity_id",
 *     "uuid",
 *     "settings"
 *   },
 *   links = {}
 * )
 */
class DomainFieldSettings extends ConfigEntityBase implements DomainFieldSettingsInterface {

  /**
   * The field settings ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The list with all settings.
   *
   * @var array
   */
  protected $settings;

  /**
   * The entity type id, eg. 'node_type'.
   *
   * @var string
   */
  protected $entity_type_id;

  /**
   * The entity id, eg. 'article'.
   *
   * @var string|null
   */
  protected $entity_id;

  /**
   * {@inheritdoc}
   */
  public function getTargetEntityTypeId(): string {
    return $this->entity_type_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetEntityId(): string|null {
    return $this->entity_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setSettings(array $settings) {
    $this->settings = $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function getSettings(): array {
    return $this->settings;
  }

  /**
   * {@inheritdoc}
   */
  public function getSetting(string $name): mixed {
    return $this->settings[$name];
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled(): bool {
    return $this->settings['status'];
  }

  /**
   * {@inheritdoc}
   */
  public function getAllowedDomains(): array {
    return $this->settings['allowed_domains'];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    parent::calculateDependencies();

    /** @var \Drupal\decoupled_domain\Plugin\DecoupledDomainEntityManager $pluginEntityManager */
    $pluginEntityManager = \Drupal::service('plugin.manager.decoupled_domain.entity_plugin');

    if ($this->entity_id) {
      // Create dependency on the bundle.
      $bundle = \Drupal::entityTypeManager()->getDefinition($this->entity_type_id);
      $entity_type = \Drupal::entityTypeManager()->getDefinition($bundle->getBundleOf());
      $bundle_config_dependency = $entity_type->getBundleConfigDependency($this->entity_id);
      $this->addDependency($bundle_config_dependency['type'], $bundle_config_dependency['name']);
      $def = $pluginEntityManager->createInstanceByEntityType($entity_type->id())->getPluginDefinition();
    }
    else {
      $def = $pluginEntityManager->createInstanceByEntityType($this->entity_type_id)->getPluginDefinition();
    }

    if ($def) {
      // Create dependency on the plugin.
      $this->addDependency('module', $def['provider']);
    }

    return $this;
  }

}
