<?php

namespace Drupal\decoupled_domain\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Behavior settings entities.
 */
interface DomainFieldSettingsInterface extends ConfigEntityInterface {

  /**
   * Set all settings.
   *
   * @param array $settings
   *   The list with all settings.
   */
  public function setSettings(array $settings);

  /**
   * Get all settings.
   *
   * @return array
   *   The list with all settings.
   */
  public function getSettings(): array;

  /**
   * Get single setting.
   *
   * @param string $name
   *   The name of the setting.
   *
   * @return mixed
   *   The decoupled domain field setting.
   */
  public function getSetting(string $name): mixed;

  /**
   * Get allowed domains.
   *
   * @return array
   *   The list with the allowed domains.
   */
  public function getAllowedDomains(): array;

  /**
   * Check if decoupled domain field is enabled.
   *
   * @return bool
   *   The decoupled domain field status.
   */
  public function isEnabled(): bool;

  /**
   * Get target entity type id.
   *
   * @return string
   *   The target entity type id.
   */
  public function getTargetEntityTypeId(): string;

  /**
   * Get target entity  id.
   *
   * @return string|null
   *   The target entity id.
   */
  public function getTargetEntityId(): string|null;

}
