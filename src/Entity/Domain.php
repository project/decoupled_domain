<?php

namespace Drupal\decoupled_domain\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;
use Drupal\decoupled_domain\DomainConfigPluginCollection;
use Drupal\decoupled_domain\Plugin\DecoupledDomainConfigInterface;

/**
 * Defines the Domain entity.
 *
 * @ConfigEntityType(
 *   id = "decoupled_domain",
 *   label = @Translation("Decoupled Domain"),
 *   label_collection = @Translation("Decoupled Domains"),
 *   label_singular = @Translation("decoupled domain"),
 *   label_plural = @Translation("decoupled domains"),
 *   label_count = @PluralTranslation(
 *     singular = "@count decoupled domain",
 *     plural = "@count decoupled domains",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\decoupled_domain\DomainListBuilder",
 *     "form" = {
 *       "add" = "Drupal\decoupled_domain\Form\DomainAddForm",
 *       "edit" = "Drupal\decoupled_domain\Form\DomainEditForm",
 *       "delete" = "Drupal\decoupled_domain\Form\DomainDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\decoupled_domain\DomainHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "domain",
 *   admin_permission = "administer decoupled domains",
 *   entity_keys = {
 *     "id" = "name",
 *     "label" = "label"
 *   },
 *   config_export = {
 *     "name",
 *     "label",
 *     "domains",
 *     "configs"
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/decoupled-domain/add",
 *     "edit-form" = "/admin/structure/decoupled-domain/{decoupled_domain}",
 *     "delete-form" = "/admin/structure/decoupled-domain/{decoupled_domain}/delete",
 *     "collection" = "/admin/structure/decoupled-domain"
 *   }
 * )
 */
class Domain extends ConfigEntityBase implements DomainInterface, EntityWithPluginCollectionInterface {

  /**
   * The Domain name.
   *
   * @var string
   */
  protected $name;

  /**
   * The Domain label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Domain names.
   *
   * @var array
   */
  protected $domains = [];

  /**
   * The Domain configs.
   *
   * @var array
   */
  protected $configs = [];

  /**
   * Holds the collection of domain configs used by this decoupled domain.
   *
   * @var \Drupal\decoupled_domain\DomainConfigPluginCollection
   */
  protected $configCollection;

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->name;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name');
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDomains() {
    return $this->domains;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultDomain() {
    foreach ($this->domains as $domain) {
      if ($domain['default']) {
        return $domain;
      }
    }
    return reset($this->domains);
  }

  /**
   * Returns the domain config plugin manager.
   *
   * @return \Drupal\Component\Plugin\PluginManagerInterface
   *   The domain config plugin manager.
   */
  protected function getDomainConfigPluginManager() {
    return \Drupal::service('plugin.manager.decoupled_domain.config');
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginCollections() {
    return ['configs' => $this->getConfigs()];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigs() {
    if (!$this->configCollection) {
      $this->configCollection = new DomainConfigPluginCollection($this->getDomainConfigPluginManager(), $this, $this->configs);
      $this->configCollection->sort();
    }
    return $this->configCollection;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig($config) {
    return $this->getConfigs()->get($config);
  }

  /**
   * {@inheritdoc}
   */
  public function addConfig(array $configuration) {
    $configuration['uuid'] = $this->uuidGenerator()->generate();
    $this->getConfigs()->addInstanceId($configuration['uuid'], $configuration);
    return $configuration['uuid'];
  }

  /**
   * {@inheritdoc}
   */
  public function deleteConfig(DecoupledDomainConfigInterface $config) {
    $this->getConfigs()->removeInstanceId($config->getUuid());
    $this->save();
    return $this;
  }

}
