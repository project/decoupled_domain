<?php

namespace Drupal\decoupled_domain\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines an interface for Decoupled Domain entity plugin plugins.
 */
interface DecoupledDomainEntityInterface extends PluginInspectionInterface {

  /**
   * Alter the extra fields added to all entities that support decoupled domain.
   *
   * @param array $fields
   *   An array with new field definition objects.
   */
  public function alterExtraFields(array $fields);

  /**
   * Extend original entity bundle form.
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\Core\Config\Entity\ConfigEntityInterface $bundle
   *   The entity bundle.
   */
  public function alterBundleEntityForm(array &$form, FormStateInterface $form_state, ConfigEntityInterface $bundle);

  /**
   * Extend original content entity form.
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   */
  public function alterEntityForm(array &$form, FormStateInterface $form_state, ContentEntityInterface $entity);

  /**
   * Return the form ID of the config form for this plugin's entity.
   *
   * Return the form ID of the global config form for the entity targeted by
   * this plugin.
   *
   * @return string
   *   The form ID of the global config form.
   */
  public function getGlobalConfigFormId();

  /**
   * Extend original content entity form.
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function alterGlobalConfigForm(array &$form, FormStateInterface $form_state);

}
