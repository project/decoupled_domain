<?php

namespace Drupal\decoupled_domain\Plugin;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\decoupled_domain\Annotation\DecoupledDomainEntity;

/**
 * Provides the Decoupled Domain entity extender plugin manager.
 */
class DecoupledDomainEntityManager extends DefaultPluginManager {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructor for DomainEntityPluginManager objects.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct('Plugin/DecoupledDomain/Entity', $namespaces, $module_handler, DecoupledDomainEntityInterface::class, DecoupledDomainEntity::class);

    $this->alterInfo('decoupled_domain_entity_info');
    $this->setCacheBackend($cache_backend, 'decoupled_domain_entity_plugins');

    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Get the extra fields for all entities that support decoupled domain.
   *
   * @param string $entity_type
   *   The string ID of the entity type.
   */
  public function getBasicExtraFields($entity_type) {
    $fields = [];
    $fields['decoupled_domain'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Domain'))
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setDescription(t('The domains the node is part of.'))
      ->setSetting('target_type', 'decoupled_domain');
    $fields['decoupled_domain_canonical'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Canonical Domain'))
      ->setDescription(t('The canonical domain for the node.'))
      ->setSetting('target_type', 'decoupled_domain');

    $this->createInstanceByEntityType($entity_type)->alterExtraFields($fields);

    return $fields;
  }

  /**
   * Build the configuration form.
   */
  private function buildConfigForm($form, FormStateInterface $form_state, $entity_type_id) {
    $domains = $this->getDomains();
    $options = [];
    foreach ($domains as $domain) {
      $options[$domain->id()] = $domain->label();
    }

    $form['decoupled_domain'] = [
      '#type' => 'details',
      '#title' => t('Decoupled Domain'),
      '#collapsed' => FALSE,
      '#collapsible' => TRUE,
      '#tree' => TRUE,
      '#weight' => 10,
      '#group' => 'additional_settings',
      '#attributes' => ['class' => ['decoupled-domain-settings-form']],
    ];
    $form['decoupled_domain']['status'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable decoupled domain for this entity bundle'),
      '#attributes' => ['class' => ['decoupled-domain-settings-status']],
    ];
    $form['decoupled_domain']['allowed_domains'] = [
      '#type' => 'checkboxes',
      '#title' => t('Allowed domains'),
      '#options' => $options,
      '#description' => t('Leave empty to allow all.'),
      '#attributes' => ['class' => ['decoupled-domain-settings-allowed-domains']],
    ];
    $form['decoupled_domain']['entity_type_id'] = [
      '#type' => 'value',
      '#value' => $entity_type_id,
    ];

    return $form;
  }

  /**
   * Extend original entity bundle form.
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\Core\Config\Entity\ConfigEntityInterface $bundle
   *   The entity bundle.
   */
  public function alterBundleEntityForm(array &$form, FormStateInterface $form_state, ConfigEntityInterface $bundle) {
    $form = $this->buildConfigForm($form, $form_state, $bundle->getEntityTypeId());

    $settings = $this->getSettings($bundle->getEntityTypeId(), $bundle->id());
    foreach ($settings as $name => $setting) {
      if (isset($form['decoupled_domain'][$name])) {
        $form['decoupled_domain'][$name]['#default_value'] = $setting;
      }
    }

    $this->createInstanceByEntityType($bundle->getEntityType()->getBundleOf())->alterBundleEntityForm($form, $form_state, $bundle);
  }

  /**
   * Extend original entity bundle form.
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   */
  public function alterEntityForm(array &$form, FormStateInterface $form_state, ContentEntityInterface $entity) {
    $entity_bundle_id = $entity->getEntityType()->getBundleEntityType();
    $current_value = $form_state->getValue('decoupled_domain', FALSE);
    if ($current_value && isset($current_value['domains'])) {
      $current_value = array_filter($current_value['domains']);
    }

    if ($entity_bundle_id) {
      $settings = $this->getSettings($entity_bundle_id, $entity->bundle());
    }
    else {
      $settings = $this->getSettings($entity->getEntityTypeId());
    }
    $enabled = $settings['status'];
    $allowed_domains = $settings['allowed_domains'];
    $stored_values = array_map(function ($value) {
      return $value['target_id'];
    }, $entity->get('decoupled_domain')->getValue());
    $canonical_domain = !$entity->get('decoupled_domain_canonical')->isEmpty()
      ? $entity->get('decoupled_domain_canonical')->target_id : NULL;

    if (!$enabled) {
      return;
    }
    $options = [];
    foreach ($this->getDomains() as $domain) {
      if ($domain->access('view')) {
        $options[$domain->id()] = $domain->label();
      }
    }

    if (!empty($allowed_domains)) {
      $options = array_intersect_key($options, array_flip($allowed_domains));
    }

    $form['decoupled_domain'] = [
      '#type' => 'details',
      '#title' => t('Decoupled Domain'),
      '#collapsed' => FALSE,
      '#collapsible' => TRUE,
      '#tree' => TRUE,
      '#weight' => 10,
      '#group' => 'advanced',
      '#attributes' => ['class' => ['decoupled-domain-settings-form']],
    ];
    $form['decoupled_domain']['domains'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Allowed domains'),
      '#options' => $options,
      '#default_value' => $stored_values,
      '#ajax' => [
        'event' => 'change',
        'wrapper' => 'canonical-decoupled-domain-wrapper',
        'callback' => "decoupled_domain_canonical_ajax_callback",
      ],
    ];
    $canonical_options = $options;
    if ($current_value) {
      $canonical_options = array_intersect_key($options, $current_value);
    }
    elseif (!empty($stored_values) && $current_value === FALSE) {
      $canonical_options = array_intersect_key($options, array_flip($stored_values));
    }
    $form['decoupled_domain']['canonical'] = [
      '#type' => 'radios',
      '#title' => $this->t('Canonical domain'),
      '#options' => $canonical_options,
      '#default_value' => $canonical_domain,
      '#prefix' => '<div id="canonical-decoupled-domain-wrapper">',
      '#suffix' => '</div>',
    ];
    $form['#entity_builders'][] = 'decoupled_domain_entity_builder';

    $this->createInstanceByEntityType($entity->getEntityTypeId())->alterEntityForm($form, $form_state, $entity);
  }

  /**
   * Extend original entity bundle form.
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string $entity_type_id
   *   The entity type id.
   */
  public function alterGlobalConfigForm(array &$form, FormStateInterface $form_state, $entity_type_id) {
    $form = $this->buildConfigForm($form, $form_state, $entity_type_id);

    $settings = $this->getSettings($entity_type_id);
    foreach ($settings as $name => $setting) {
      if (isset($form['decoupled_domain'][$name])) {
        $form['decoupled_domain'][$name]['#default_value'] = $setting;
      }
    }

    $this->createInstanceByEntityType($entity_type_id)->alterGlobalConfigForm($form, $form_state);
  }

  /**
   * Get all available decoupled domains.
   *
   * @return \Drupal\decoupled_domain\Entity\DomainInterface[]
   *   Array of all available domains.
   */
  private function getDomains() {
    return $this->entityTypeManager
      ->getStorage('decoupled_domain')
      ->loadMultiple();
  }

  /**
   * Retrieve field settings.
   *
   * @param string $entity_type_id
   *   The entity type id, eg. 'node_type'.
   * @param string|null $entity_id
   *   The entity id, eg. 'article'.
   *
   * @return array|null
   *   The list with the field settings.
   */
  private function getSettings(string $entity_type_id, string $entity_id = NULL) {
    $id = $entity_type_id . (isset($entity_id) ? '_' . $entity_id : '');
    $entity = $this->entityTypeManager
      ->getStorage('decoupled_domain_field_settings')
      ->load($id);
    return isset($entity) ? $entity->getSettings() : NULL;
  }

  /**
   * Create an instance of the first plugin found with string id $entity_type.
   *
   * Create an instance of the first plugin found supporting the entity type
   * with string id $entity_type.
   *
   * @param string $entity_type
   *   The string ID of the entity type.
   *
   * @return \Drupal\decoupled_domain\Plugin\DecoupledDomainEntityInterface
   *   The plugin.
   */
  public function createInstanceByEntityType($entity_type) {
    $plugin_ids = array_keys($this->loadDefinitionsByEntityType($entity_type));
    return $this->createInstance($plugin_ids[0]);
  }

  /**
   * Load plugins implementing entity with id $entity_type.
   *
   * @param string $entity_type
   *   The string ID of the entity type.
   *
   * @return array
   *   An array of plugin definitions for the entity type with ID $entity_type.
   */
  public function loadDefinitionsByEntityType($entity_type) {
    return array_filter($this->getDefinitions(), function ($var) use ($entity_type) {
      return $var['entityType'] == $entity_type;
    });
  }

  /**
   * Load the string IDs for the supported entity types.
   *
   * @return array
   *   An array of entity type ID strings.
   */
  public function loadSupportedEntityTypes() {
    return array_values(array_map(function ($var) {
      return $var['entityType'];
    }, $this->getDefinitions()));
  }

  /**
   * Load the string IDs for the supported bundle entity types.
   *
   * @return array
   *   An array of entity type ID strings.
   */
  public function loadSupportedBundleEntityTypes() {
    return array_values(array_map(function ($var) {
      return $this->entityTypeManager->getStorage($var['entityType'])
        ->getEntityType()->getBundleEntityType();
    }, $this->getDefinitions()));
  }

  /**
   * Load the string IDs for the global configuration forms for entity types.
   *
   * @return array
   *   An array of entity types and form ID strings in the form
   *   form_id => entity_type.
   */
  public function loadSupportedGlobalForms() {
    $result = [];
    foreach ($this->getDefinitions() as $key => $def) {
      $form_id = $this->createInstance($key)->getGlobalConfigFormId();
      if (isset($form_id)) {
        $result[$form_id] = $def['entityType'];
      }
    }
    return $result;
  }

}
