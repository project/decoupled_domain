<?php

namespace Drupal\decoupled_domain\Plugin;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a base class for configurable image effects.
 *
 * @see \Drupal\decoupled_domain\Annotation\DecoupledDomainConfig
 * @see \Drupal\decoupled_domain\Plugin\ConfigurableDecoupledDomainConfigInterface
 * @see \Drupal\decoupled_domain\Plugin\DecoupledDomainConfigInterface
 * @see \Drupal\decoupled_domain\Plugin\DecoupledDomainConfigBase
 * @see \Drupal\decoupled_domain\Plugin\DecoupledDomainConfigManager
 * @see plugin_api
 */
abstract class ConfigurableDecoupledDomainConfigBase extends DecoupledDomainConfigBase implements ConfigurableDecoupledDomainConfigInterface {

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

}
