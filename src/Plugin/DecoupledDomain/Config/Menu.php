<?php

namespace Drupal\decoupled_domain\Plugin\DecoupledDomain\Config;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\decoupled_domain\Plugin\ConfigurableDecoupledDomainConfigBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Domain config for site menu.
 *
 * @DecoupledDomainConfig(
 *  id = "menu",
 *  label = @Translation("Menu"),
 *  multiple = TRUE
 * )
 */
class Menu extends ConfigurableDecoupledDomainConfigBase implements ContainerFactoryPluginInterface {

  public const CREATE_NEW = ':create_new';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManger = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $menus = $this->getAllMenuEntities();

    $options = [];
    $options[static::CREATE_NEW] = $this->t('- Create new -');
    foreach ($menus as $menu) {
      $options[$menu->id()] = $menu->label();
    }

    $form['menu_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Select menu'),
      '#description' => $this->t('Select existing menu or create new one.'),
      '#options' => $options,
      '#required' => TRUE,
      '#default_value' => $this->configuration['menu_id'],
    ];

    $form['name'] = [
      '#type' => 'machine_name',
      '#machine_name' => [
        'source' => ['data', 'menu_id'],
        'exists' => [$this, 'exists'],
      ],
      '#disabled' => isset($this->configuration['name']) && !empty($this->configuration['name']),
      '#default_value' => $this->configuration['name'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['menu_id'] = $form_state->getValue('menu_id');
    $this->configuration['name'] = $form_state->getValue('name');
  }

  /**
   * Retrieve all menu entities.
   *
   * @return \Drupal\system\MenuInterface[]
   *   List with all menu entities.
   */
  private function getAllMenuEntities() {
    return $this->entityTypeManger
      ->getStorage('menu')
      ->loadMultiple();
  }

  /**
   * Check if the name already exists.
   *
   * @param string $id
   *   The name.
   *
   * @return bool
   *   Is this name already taken.
   */
  public function exists(string $id) {
    foreach ($this->domain->getConfigs()->getConfiguration() as $config) {
      if ($config['id'] === $this->getPluginId() && $config['data']['name'] === $id) {
        return TRUE;
      }
    }
    return FALSE;
  }

}
