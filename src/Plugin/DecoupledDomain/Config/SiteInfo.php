<?php

namespace Drupal\decoupled_domain\Plugin\DecoupledDomain\Config;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\decoupled_domain\Plugin\ConfigurableDecoupledDomainConfigBase;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Domain config for site name.
 *
 * @DecoupledDomainConfig(
 *  id = "site_info",
 *  label = @Translation("Site information")
 * )
 */
class SiteInfo extends ConfigurableDecoupledDomainConfigBase implements ContainerFactoryPluginInterface {

  /**
   * Node type storage service.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $nodeTypeStorage;

  /**
   * Node type storage service.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $nodeStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->nodeTypeStorage = $container->get('entity_type.manager')->getStorage('node_type');
    $instance->nodeStorage = $container->get('entity_type.manager')->getStorage('node');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $contentTypes = $this->nodeTypeStorage->loadMultiple();
    $options = ['' => $this->t("- None -")];
    foreach ($contentTypes as $contentType) {
      $options[$contentType->id()] = $contentType->label();
    }

    $form['site_information'] = [
      '#type' => 'details',
      '#title' => $this->t('Site details'),
      '#open' => TRUE,
    ];
    $form['site_information']['site_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site name'),
      '#default_value' => $this->configuration['site_name'],
      '#required' => TRUE,
    ];
    $form['site_information']['site_slogan'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Slogan'),
      '#default_value' => $this->configuration['site_slogan'],
      '#description' => $this->t("How this is used depends on your site's theme."),
      '#maxlength' => 255,
    ];

    $form['front_page'] = [
      '#type' => 'details',
      '#title' => $this->t('Front page'),
      '#open' => TRUE,
    ];
    $form['front_page']['site_frontpage'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default front page'),
      '#default_value' => $this->configuration['site_frontpage'],
      '#size' => 40,
      '#description' => $this->t('Optionally, specify a relative URL to display as the front page.'),
    ];
    $form['front_page']['node_type'] = [
      '#type' => 'select',
      '#options' => $options,
      '#title' => $this->t('Or create new'),
      '#description' => $this->t('A new node will be created with the name of the domain as a title and assigned to it. This is only available when creating a new domain.'),
      '#disabled' => !empty($this->configuration['site_frontpage']),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $site_information = $form_state->getValue('site_information');
    $front_page = $form_state->getValue('front_page');
    $this->configuration['site_name'] = $site_information['site_name'];
    $this->configuration['site_slogan'] = $site_information['site_slogan'];
    if ($type = $front_page['node_type']) {
      $node = $this->nodeStorage->create([
        'type' => $type,
        'title' => $this->domain->label() . ' home',
        'decoupled_domain' => $this->domain->id(),
        'promote' => NodeInterface::NOT_PROMOTED,
        'uid' => \Drupal::currentUser()->id(),
        'status' => NodeInterface::PUBLISHED,
        'moderation_state' => 'published',
      ]);
      $node->save();
      $this->configuration['site_frontpage'] = $node->toUrl()->toString();
    }
    else {
      $this->configuration['site_frontpage'] = $front_page['site_frontpage'];
    }
  }

}
