<?php

namespace Drupal\decoupled_domain\Plugin;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\decoupled_domain\Entity\DomainInterface;

/**
 * Defines an interface for Domain config plugins.
 */
interface DecoupledDomainConfigInterface extends PluginInspectionInterface, ConfigurableInterface, DependentPluginInterface {

  /**
   * Returns the domain config label.
   *
   * @return string
   *   The domain config label.
   */
  public function label();

  /**
   * Returns whether the config can be used multiple times for single domain.
   *
   * @return bool
   *   TRUE if it can be used for single decoupled domain.
   *   FALSE if it can be used only one time for single decoupled domain.
   */
  public function handlesMultipleUses();

  /**
   * Returns the unique ID representing the image effect.
   *
   * @return string
   *   The image effect ID.
   */
  public function getUuid();

  /**
   * Set the decoupled domain this configuration is part of.
   *
   * @param \Drupal\decoupled_domain\Entity\DomainInterface $domain
   *   The decoupled domain entity.
   */
  public function setDomain(DomainInterface $domain);

}
