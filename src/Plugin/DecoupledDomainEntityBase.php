<?php

namespace Drupal\decoupled_domain\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Base class for Decoupled Domain entity plugin plugins.
 */
abstract class DecoupledDomainEntityBase extends PluginBase implements DecoupledDomainEntityInterface {

  /**
   * {@inheritdoc}
   */
  public function alterExtraFields(array $fields) {
    // Alter the extra fields.
  }

  /**
   * {@inheritdoc}
   */
  public function alterBundleEntityForm(array &$form, FormStateInterface $formState, ConfigEntityInterface $bundle) {
    // Extend the entity bundle form.
  }

  /**
   * {@inheritdoc}
   */
  public function alterEntityForm(array &$form, FormStateInterface $formState, ContentEntityInterface $entity) {
    // Extend the content entity form.
  }

  /**
   * {@inheritdoc}
   */
  public function getGlobalConfigFormId() {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function alterGlobalConfigForm(array &$form, FormStateInterface $formState) {
    // Extend the global form.
  }

}
