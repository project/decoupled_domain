<?php

namespace Drupal\decoupled_domain\Plugin;

use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Defines the interface for configurable image effects.
 *
 * @see \Drupal\decoupled_domain\Annotation\DecoupledDomainConfig
 * @see \Drupal\decoupled_domain\Plugin\DecoupledDomainConfigInterface
 * @see \Drupal\decoupled_domain\Plugin\DecoupledDomainConfigBase
 * @see \Drupal\decoupled_domain\Plugin\DecoupledDomainConfigManager
 * @see plugin_api
 */
interface ConfigurableDecoupledDomainConfigInterface extends DecoupledDomainConfigInterface, PluginFormInterface {
}
