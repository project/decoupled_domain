<?php

namespace Drupal\decoupled_domain\Plugin;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\decoupled_domain\Annotation\DecoupledDomainConfig;

/**
 * Provides the Decoupled Domain config plugin manager.
 */
class DecoupledDomainConfigManager extends DefaultPluginManager {

  use DependencySerializationTrait;

  /**
   * Constructs a new DecoupledDomainConfigManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/DecoupledDomain/Config', $namespaces, $module_handler, DecoupledDomainConfigInterface::class, DecoupledDomainConfig::class);

    $this->alterInfo('decoupled_domain_config_info');
    $this->setCacheBackend($cache_backend, 'decoupled_domain_config_plugins');
  }

}
