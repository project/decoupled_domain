<?php

namespace Drupal\decoupled_domain\Plugin;

use Drupal\Core\Plugin\PluginBase;
use Drupal\decoupled_domain\Entity\DomainInterface;

/**
 * Base class for Domain config plugins.
 */
abstract class DecoupledDomainConfigBase extends PluginBase implements DecoupledDomainConfigInterface {

  /**
   * The image effect ID.
   *
   * @var string
   */
  protected $uuid;

  /**
   * The decoupled domain this configuration is part of.
   *
   * @var \Drupal\decoupled_domain\Entity\DomainInterface
   */
  protected $domain;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function handlesMultipleUses() {
    $definition = $this->getPluginDefinition();
    return $definition['multiple'];
  }

  /**
   * {@inheritdoc}
   */
  public function getUuid() {
    return $this->uuid;
  }

  /**
   * {@inheritdoc}
   */
  public function setDomain(DomainInterface $domain) {
    $this->domain = $domain;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return [
      'uuid' => $this->getUuid(),
      'id' => $this->getPluginId(),
      'data' => $this->configuration,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $configuration += [
      'data' => [],
      'uuid' => '',
    ];
    $this->configuration = $configuration['data'] + $this->defaultConfiguration();
    $this->uuid = $configuration['uuid'];
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    return [];
  }

}
