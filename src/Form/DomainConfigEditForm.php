<?php

namespace Drupal\decoupled_domain\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\decoupled_domain\Entity\DomainInterface;

/**
 * Provides an edit form for domain config.
 *
 * @internal
 */
class DomainConfigEditForm extends DomainConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, DomainInterface $decoupled_domain = NULL, $domain_config = NULL) {
    $form = parent::buildForm($form, $form_state, $decoupled_domain, $domain_config);

    $form['#title'] = $this->t('Edit %label config for domain %domain', [
      '%label' => $this->config->label(),
      '%domain' => $decoupled_domain->label(),
    ]);
    $form['actions']['submit']['#value'] = $this->t('Update config');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareDomainConfig($config) {
    return $this->domain->getConfig($config);
  }

}
