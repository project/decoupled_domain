<?php

namespace Drupal\decoupled_domain\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\decoupled_domain\Plugin\ConfigurableDecoupledDomainConfigInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for decoupled domain addition forms.
 *
 * @internal
 */
class DomainEditForm extends DomainFormBase {

  /**
   * The domain config manager service.
   *
   * @var \Drupal\decoupled_domain\Plugin\DecoupledDomainConfigManager
   */
  protected $domainConfigManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->domainConfigManager = $container->get('plugin.manager.decoupled_domain.config');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

    $form['configs'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Config'),
        $this->t('Operations'),
      ],
      '#empty' => $this->t('There are currently no configurations for this domain. Add one by selecting an option below.'),
      '#weight' => 5,
    ];

    foreach ($this->entity->getConfigs() as $config) {
      $key = $config->getUuid();
      $form['configs'][$key]['config'] = [
        '#tree' => FALSE,
        'data' => [
          'label' => [
            '#plain_text' => $config->label(),
          ],
        ],
      ];

      $links = [];
      $is_configurable = $config instanceof ConfigurableDecoupledDomainConfigInterface;
      if ($is_configurable) {
        $links['edit'] = [
          'title' => $this->t('Edit'),
          'url' => Url::fromRoute('decoupled_domain.config_edit_form', [
            'decoupled_domain' => $this->entity->id(),
            'domain_config' => $key,
          ]),
        ];
      }
      $links['delete'] = [
        'title' => $this->t('Delete'),
        'url' => Url::fromRoute('decoupled_domain.config_delete', [
          'decoupled_domain' => $this->entity->id(),
          'domain_config' => $key,
        ]),
      ];
      $form['configs'][$key]['operations'] = [
        '#type' => 'operations',
        '#links' => $links,
      ];
    }

    $new_config_options = [];
    $plugins = $this->domainConfigManager->getDefinitions();
    foreach ($plugins as $plugin_id => $definition) {
      $count = array_reduce($this->entity->getConfigs()->getConfiguration(), function ($count, $config) use ($plugin_id) {
        if ($config['id'] === $plugin_id) {
          return $count + 1;
        }
        return $count;
      }, 0);
      if (($definition['multiple'] ?? FALSE) || $count === 0) {
        $new_config_options[$plugin_id] = $definition['label'];
      }
    }

    $form['configs']['new'] = [
      '#tree' => FALSE,
    ];
    $form['configs']['new']['config'] = [
      'data' => [
        'new' => [
          '#type' => 'select',
          '#title' => $this->t('Config'),
          '#title_display' => 'invisible',
          '#options' => $new_config_options,
          '#empty_option' => $this->t('Select a new configuration'),
        ],
        [
          'add' => [
            '#type' => 'submit',
            '#value' => $this->t('Add'),
            '#validate' => ['::configValidate'],
            '#submit' => ['::submitForm', '::configSave'],
          ],
        ],
      ],
      '#prefix' => '<div class="container-inline">',
      '#suffix' => '</div>',
    ];

    $form['configs']['new']['operations'] = [
      'data' => [],
    ];

    return parent::form($form, $form_state);
  }

  /**
   * Validate handler for domain config.
   */
  public function configValidate($form, FormStateInterface $form_state) {
    if (!$form_state->getValue('new')) {
      $form_state->setErrorByName('new', $this->t('Select an config to add.'));
    }
  }

  /**
   * Submit handler for domain config.
   */
  public function configSave($form, FormStateInterface $form_state) {
    $this->save($form, $form_state);

    // Check if this field has any configuration options.
    $config = $this->domainConfigManager->getDefinition($form_state->getValue('new'));

    // Load the configuration form for this option.
    if (is_subclass_of($config['class'], '\Drupal\decoupled_domain\Plugin\ConfigurableDecoupledDomainConfigInterface')) {
      $form_state->setRedirect(
        'decoupled_domain.config_add_form',
        [
          'decoupled_domain' => $this->entity->id(),
          'domain_config' => $form_state->getValue('new'),
        ],
      );
    }
    // If there's no form, immediately add the domain config.
    else {
      $config = [
        'id' => $config['id'],
        'data' => [],
      ];
      $config_id = $this->entity->addConfig($config);
      $this->entity->save();
      if (!empty($config_id)) {
        $this->messenger()->addStatus($this->t('The domain config was successfully saved.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    parent::save($form, $form_state);
    $this->messenger()->addStatus($this->t('Changes to the domain have been saved.'));
  }

}
