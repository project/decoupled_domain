<?php

namespace Drupal\decoupled_domain\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DomainForm.
 */
class DomainFormBase extends EntityForm {
  /**
   * The entity being used by this form.
   *
   * @var \Drupal\decoupled_domain\Entity\DomainInterface
   */
  protected $entity;

  /**
   * The decoupled domain entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static();
    $instance->storage = $container->get('entity_type.manager')->getStorage('decoupled_domain');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Domain name'),
      '#default_value' => $this->entity->label(),
      '#required' => TRUE,
    ];
    $form['name'] = [
      '#type' => 'machine_name',
      '#machine_name' => [
        'exists' => [$this->storage, 'load'],
      ],
      '#default_value' => $this->entity->id(),
      '#required' => TRUE,
    ];

    $domains = $form_state->getValue('domains', $this->entity->getDomains());

    $header = [
      $this->t('Protocol'),
      $this->t('Domain'),
      $this->t('Port'),
      $this->t('Default'),
      $this->t('Operations'),
    ];

    $form['domain_list'] = [
      '#type' => 'details',
      '#title' => $this->t('Domain list'),
      '#id' => 'domain-list',
      '#open' => empty($domains) || $form_state->get('domain_list_active'),
    ];

    $form['domain_list']['domains'] = [
      '#type' => 'table',
      '#three' => TRUE,
      '#header' => $header,
      '#empty' => $this->t('No domains'),
    ];

    foreach ($domains as $key => $domain) {
      $form['domain_list']['domains'][$key] = [
        'protocol' => [
          '#type' => 'select',
          '#options' => [
            'http' => $this->t('http://'),
            'https' => $this->t('https://'),
          ],
          '#default_value' => $domain['protocol'] ?? "http",
        ],
        'domain' => [
          '#type' => 'textfield',
          '#default_value' => $domain['domain'] ?? "",
        ],
        'port' => [
          '#type' => 'textfield',
          '#maxlength' => 4,
          '#size' => 4,
          '#attributes' => [
            'placeholder' => "80",
          ],
          '#default_value' => $domain['port'],
        ],
        'default' => [
          '#type' => 'value',
          '#value' => $domain['default'] ?? FALSE,
          'label' => [
            '#markup' => $domain['default'] ? $this->t("Yes") : $this->t("No"),
          ],
        ],
        'operations' => [
          'delete' => [
            '#type' => 'submit',
            '#value' => $this->t('delete'),
            '#button_type' => 'danger',
            '#submit' => ['::deleteDomain'],
            '#parents' => ['domain_list', $key],
            '#ajax' => [
              'callback' => '::updateDomainListCallback',
              'wrapper' => 'domain-list',
            ],
          ],
          'make_default' => [
            '#type' => 'submit',
            '#value' => $this->t('make default'),
            '#access' => !($domain['default'] ?? FALSE),
            '#submit' => ['::setDefaultDomain'],
            '#parents' => ['domain_list', $key],
            '#ajax' => [
              'callback' => '::updateDomainListCallback',
              'wrapper' => 'domain-list',
            ],
          ],
        ],
      ];
    }

    $form['domain_list']['add-domain'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add more'),
      '#submit' => ['::addDomain'],
      '#ajax' => [
        'callback' => '::updateDomainListCallback',
        'wrapper' => 'domain-list',
      ],
    ];

    return parent::form($form, $form_state);
  }

  /**
   * Submit handler to add additional host address.
   */
  public function addDomain(array &$form, FormStateInterface $form_state) {
    $domains = $form_state->getValue('domains', []);

    if (!is_array($domains)) {
      $domains = [];
    }

    $domains[] = [
      'protocol' => 'http',
      'domain' => '',
      'port' => 80,
      'default' => FALSE,
    ];
    $form_state->setValue('domains', $domains);
    $form_state->set('domain_list_active', TRUE);
    $form_state->setRebuild();
  }

  /**
   * Submit handler to delete a host address.
   */
  public function deleteDomain(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    $row = $trigger['#parents'][1];
    $domains = $form_state->getValue('domains', []);
    unset($domains[$row]);
    $form_state->setValue('domains', $domains);
    $form_state->set('domain_list_active', TRUE);
    $form_state->setRebuild();
  }

  /**
   * Submit handler to set a host address as default.
   */
  public function setDefaultDomain(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    $row = $trigger['#parents'][1];
    $domains = $form_state->getValue('domains', []);
    foreach ($domains as $key => &$domain) {
      $domain['default'] = $key === $row;
    }
    $form_state->setValue('domains', $domains);
    $form_state->set('domain_list_active', TRUE);
    $form_state->setRebuild();
  }

  /**
   * Ajax callback.
   */
  public function updateDomainListCallback(array &$form, FormStateInterface $form_state) {
    return $form['domain_list'];
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    parent::save($form, $form_state);
    $form_state->setRedirectUrl($this->entity->toUrl('edit-form'));
  }

}
