<?php

namespace Drupal\decoupled_domain\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\decoupled_domain\Entity\DomainInterface;

/**
 * Form for deleting a domain config.
 *
 * @internal
 */
class DomainConfigDeleteForm extends ConfirmFormBase {

  /**
   * The decoupled domain containing the domain config to be deleted.
   *
   * @var \Drupal\decoupled_domain\Entity\DomainInterface
   */
  protected $domain;

  /**
   * The domain config to be deleted.
   *
   * @var \Drupal\decoupled_domain\Plugin\DecoupledDomainConfigInterface
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the @config config from the %domain domain?', [
      '%domain' => $this->domain->label(),
      '@config' => $this->config->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return $this->domain->toUrl('edit-form');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'domain_config_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, DomainInterface $decoupled_domain = NULL, $domain_config = NULL) {
    $this->domain = $decoupled_domain;
    $this->config = $this->domain->getConfig($domain_config);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->domain->deleteConfig($this->config);
    $this->messenger()->addStatus($this->t('The domain config %name has been deleted.', ['%name' => $this->config->label()]));
    $form_state->setRedirectUrl($this->domain->toUrl('edit-form'));
  }

}
