<?php

namespace Drupal\decoupled_domain\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\decoupled_domain\Plugin\ConfigurableDecoupledDomainConfigInterface;
use Drupal\decoupled_domain\Entity\DomainInterface;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides a base form for domain config.
 */
abstract class DomainConfigFormBase extends FormBase {

  /**
   * The decoupled domain.
   *
   * @var \Drupal\decoupled_domain\Entity\DomainInterface
   */
  protected $domain;

  /**
   * The domain config.
   *
   * @var \Drupal\decoupled_domain\Plugin\DecoupledDomainConfigInterface|\Drupal\decoupled_domain\Plugin\ConfigurableDecoupledDomainConfigInterface
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'domain_config_form';
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\decoupled_domain\Entity\DomainInterface $decoupled_domain
   *   The decoupled domain.
   * @param string $domain_config
   *   The domain config ID.
   *
   * @return array
   *   The form structure.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   */
  public function buildForm(array $form, FormStateInterface $form_state, DomainInterface $decoupled_domain = NULL, $domain_config = NULL) {
    $this->domain = $decoupled_domain;
    try {
      $this->config = $this->prepareDomainConfig($domain_config);
    }
    catch (PluginNotFoundException $e) {
      throw new NotFoundHttpException("Invalid config id: '$domain_config'.");
    }
    $request = $this->getRequest();

    if (!($this->config instanceof ConfigurableDecoupledDomainConfigInterface)) {
      throw new NotFoundHttpException();
    }

    $form['uuid'] = [
      '#type' => 'value',
      '#value' => $this->config->getUuid(),
    ];
    $form['id'] = [
      '#type' => 'value',
      '#value' => $this->config->getPluginId(),
    ];

    $form['data'] = [];
    $subform_state = SubformState::createForSubform($form['data'], $form, $form_state);
    $form['data'] = $this->config->buildConfigurationForm($form['data'], $subform_state);
    $form['data']['#tree'] = TRUE;

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
    ];
    $form['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#url' => $this->domain->toUrl('edit-form'),
      '#attributes' => ['class' => ['button']],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $this->config->validateConfigurationForm($form['data'], SubformState::createForSubform($form['data'], $form, $form_state));
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->cleanValues();

    $this->config->submitConfigurationForm($form['data'], SubformState::createForSubform($form['data'], $form, $form_state));

    if (!$this->config->getUuid()) {
      $this->domain->addConfig($this->config->getConfiguration());
    }
    $this->domain->save();

    $this->messenger()->addStatus($this->t('The domain config was successfully saved.'));
    $form_state->setRedirectUrl($this->domain->toUrl('edit-form'));
  }

  /**
   * Converts a domain config ID into an object.
   *
   * @param string $config
   *   The domain config ID.
   *
   * @return \Drupal\decoupled_domain\Plugin\DecoupledDomainConfigInterface
   *   The domain config object.
   */
  abstract protected function prepareDomainConfig(string $config);

}
