<?php

namespace Drupal\decoupled_domain\Form;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\decoupled_domain\Entity\DomainInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an add form for image effects.
 *
 * @internal
 */
class DomainConfigAddForm extends DomainConfigFormBase {

  /**
   * The domain config manager.
   *
   * @var \Drupal\decoupled_domain\Plugin\DecoupledDomainConfigManager
   */
  protected $configManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static();
    $instance->configManager = $container->get('plugin.manager.decoupled_domain.config');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, DomainInterface $decoupled_domain = NULL, $domain_config = NULL) {
    $form = parent::buildForm($form, $form_state, $decoupled_domain, $domain_config);

    $form['#title'] = $this->t('Add %label config to domain %domain', [
      '%label' => $this->config->label(),
      '%domain' => $decoupled_domain->label(),
    ]);
    $form['actions']['submit']['#value'] = $this->t('Add config');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareDomainConfig($config) {
    $instance = $this->configManager->createInstance($config);
    $instance->setDomain($this->domain);
    return $instance;
  }

  /**
   * Checks access.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   * @param \Drupal\decoupled_domain\Entity\DomainInterface $decoupled_domain
   *   The decoupled domain.
   * @param string $domain_config
   *   The domain config ID.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account, DomainInterface $decoupled_domain, $domain_config) {
    $this->domain = $decoupled_domain;
    $instance = $this->prepareDomainConfig($domain_config);

    $count = array_reduce($decoupled_domain->getConfigs()->getConfiguration(), function ($count, $config) use ($instance) {
      if ($config['id'] === $instance->getPluginId()) {
        return $count + 1;
      }
      return $count;
    }, 0);

    return AccessResult::allowedIf($count === 0 || $instance->handlesMultipleUses());
  }

}
