<?php

namespace Drupal\decoupled_domain\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Controller for decoupled domain addition forms.
 *
 * @internal
 */
class DomainAddForm extends DomainFormBase {

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->messenger()->addStatus($this->t('Domain %name was created.', ['%name' => $this->entity->label()]));
  }

  /**
   * {@inheritdoc}
   */
  public function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Create new domain');

    return $actions;
  }

}
