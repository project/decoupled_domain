<?php

namespace Drupal\decoupled_domain;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Plugin\DefaultLazyPluginCollection;
use Drupal\decoupled_domain\Entity\DomainInterface;

/**
 * A collection of domain configurations.
 */
class DomainConfigPluginCollection extends DefaultLazyPluginCollection {

  /**
   * The decoupled domain entity.
   *
   * @var \Drupal\decoupled_domain\Entity\DomainInterface
   */
  protected $domain;

  /**
   * Constructs a new DomainConfigPluginCollection object.
   *
   * @param \Drupal\Component\Plugin\PluginManagerInterface $manager
   *   The manager to be used for instantiating plugins.
   * @param \Drupal\decoupled_domain\Entity\DomainInterface $domain
   *   The decoupled domain this configuration is part of.
   * @param array $configurations
   *   (optional) An associative array containing the initial configuration for
   *   each plugin in the collection, keyed by plugin instance ID.
   */
  public function __construct(PluginManagerInterface $manager, DomainInterface $domain, array $configurations = []) {
    parent::__construct($manager, $configurations);
    $this->domain = $domain;
  }

  /**
   * {@inheritdoc}
   *
   * @return \Drupal\decoupled_domain\Plugin\DecoupledDomainConfigInterface
   *   Single Decoupled domain configuration.
   */
  public function &get($instance_id) {
    return parent::get($instance_id);
  }

  /**
   * {@inheritdoc}
   */
  protected function initializePlugin($instance_id) {
    $configuration = $this->configurations[$instance_id] ?? [];
    if (!isset($configuration[$this->pluginKey])) {
      throw new PluginNotFoundException($instance_id);
    }
    $instance = $this->manager->createInstance($configuration[$this->pluginKey], $configuration);
    $instance->setDomain($this->domain);
    $this->set($instance_id, $instance);
  }

}
