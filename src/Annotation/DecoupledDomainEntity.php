<?php

namespace Drupal\decoupled_domain\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Domain entity plugin item annotation object.
 *
 * @see \Drupal\decoupled_domain\Plugin\DecoupledDomainEntityManager
 * @see plugin_api
 *
 * @Annotation
 */
class DecoupledDomainEntity extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The string id of the affected entity.
   *
   * @var string
   */
  public $entityType;

}
