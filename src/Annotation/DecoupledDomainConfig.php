<?php

namespace Drupal\decoupled_domain\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Domain config item annotation object.
 *
 * @see \Drupal\decoupled_domain\Plugin\DecoupledDomainConfigManager
 * @see plugin_api
 *
 * @Annotation
 */
class DecoupledDomainConfig extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the domain config.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * Does the domain handles multiple values at once.
   *
   * @var bool
   */
  public $multiple = FALSE;

}
