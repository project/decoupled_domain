# Decoupled Domain

## Introduction

The _Decoupled Domain_ module provides Domain config entities that can be used
as configurations for decoupled applications. The domains can be referenced by
content entities and that reference can be used as an api filter to access the
content only for certain host. This is especially useful if you want to control
multiple decoupled sites from single drupal installation.

## Table of content

- Introduction
- Requirements
- Installation
- Supported entity types
- Included configurations
- Sub-modules
- Configuration
- To Do
- Current Maintainers

## Requirements

No other modules are required. If you want to use the sub-modules that are
included, please check the [Sub-modules](#sub-modules) section for more
information about each sub-module requirements.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Supported entity types

This project includes sub-modules that are responsible to add decoupled domain
reference field to entities from different entity types.
The currently supported entity types are:

- Node
- User

## Included configurations

The Decoupled Domain module comes with few configuration options.
More options will be added in the future.

- Site information (_Site name, slogan, front page_)
- Menu (_Select a menu as primary, footer or other site menu_)

## Sub-modules

Decoupled Domain includes additional modules to extend the functionality
and/or for integration with other modules.
Here is a quick look what is included:

### Decoupled Domain Node

Add a domain reference field to node type entities.
Can be activated per node type.

#### Requirements

- Node
- Decoupled Domain

### Decoupled Domain User

Add a domain reference field to user entities.

#### Requirements

Drupal core modules:
- User
- Decoupled Domain

### Decoupled Domain Router

Add integration with [Decoupled Router](https://www.drupal.org/project/decoupled_router).

#### Requirements

- Decoupled Domain
- [Decoupled Router](https://www.drupal.org/project/decoupled_router)

### Decoupled Domain Access

This module is adding very rough entity access restriction
based on the domain reference field.\
This module has notting to do with the
[Domain Access](https://www.drupal.org/project/domain) module

#### Requirements

- User
- Decoupled Domain
- Decoupled Domain User

## Configuration

Navigate to Administration > Structure > Decoupled Domains.

## To Do

- Add documentation for developers.
- Add example of Decoupled Domain Config plugin.
- Add example of Decoupled Domain Entity plugin.
- Support additional Entity types.
- Support additional Decoupled Domain Configurations.
- Add tokens.

## Current Maintainers

- Petyo Stoyanov - [petyosv](https://www.drupal.org/u/petyosv)
