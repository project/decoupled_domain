<?php

namespace Drupal\decoupled_domain_user\Plugin\DecoupledDomain\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\decoupled_domain\Plugin\DecoupledDomainEntityBase;

/**
 * Implements decoupled domain behavior for users.
 *
 * @DecoupledDomainEntity(
 *  id = "user",
 *  label = @Translation("User"),
 *  entityType = "user"
 * )
 */
class User extends DecoupledDomainEntityBase {

  /**
   * {@inheritdoc}
   */
  public function getGlobalConfigFormId() {
    return "user_admin_settings";
  }

  /**
   * {@inheritdoc}
   */
  public function alterEntityForm(array &$form, FormStateInterface $formState, ContentEntityInterface $entity) {
    $form['decoupled_domain']['#access'] = \Drupal::currentUser()->hasPermission('change own domain');
  }

  /**
   * {@inheritdoc}
   */
  public function alterGlobalConfigForm(array &$form, FormStateInterface $formState) {
    $form['decoupled_domain']['#weight'] = 0;
    $form['decoupled_domain']['#open'] = TRUE;
    $form['#submit'][] = 'decoupled_domain_settings_submit';
  }

}
