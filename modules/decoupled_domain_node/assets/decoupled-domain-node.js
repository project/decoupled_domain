/**
 * @file
 */

(function ($, Drupal) {

  Drupal.behaviors.ddNode = {
    attach: function (context, settings) {

      // Display the action in the vertical tab summary.
      $(context).find('.decoupled-domain-settings-form').drupalSetSummary(function (context) {
        if ($('.decoupled-domain-settings-status', context).is(':checked')) {
          const label = [];
          $('.decoupled-domain-settings-allowed-domains:checked', context).each(function () {
            label.push(Drupal.checkPlain($(this).next('label').text()));
          });
          return label.length > 0 ? label.join(", ") : Drupal.t("All allowed");
        } else {
          return '';
        }
      });

    }
  }

})(jQuery, Drupal);
