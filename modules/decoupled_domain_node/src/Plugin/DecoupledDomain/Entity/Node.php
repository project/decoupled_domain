<?php

namespace Drupal\decoupled_domain_node\Plugin\DecoupledDomain\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\decoupled_domain\Plugin\DecoupledDomainEntityBase;

/**
 * Implements decoupled domain behavior for nodes.
 *
 * @DecoupledDomainEntity(
 *  id = "node",
 *  label = @Translation("Node"),
 *  entityType = "node"
 * )
 */
class Node extends DecoupledDomainEntityBase {

  /**
   * {@inheritdoc}
   */
  public function alterBundleEntityForm(array &$form, FormStateInterface $formState, ConfigEntityInterface $bundle) {
    $form['#attached']['library'][] = 'decoupled_domain_node/node-form';
    $form['actions']['submit']['#submit'][] = 'decoupled_domain_settings_submit';
  }

}
