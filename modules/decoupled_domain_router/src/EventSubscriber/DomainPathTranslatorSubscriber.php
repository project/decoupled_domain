<?php

namespace Drupal\decoupled_domain_router\EventSubscriber;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\path_alias\AliasManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class DomainPathTranslatorSubscriber - Event Subscriber.
 */
class DomainPathTranslatorSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * Alias Manager.
   *
   * @var \Drupal\path_alias\AliasManager
   */
  private $pathAliasManager;

  /**
   * DomainPathTranslatorSubscriber constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\path_alias\AliasManager $pathAliasManager
   *   Path Alias Manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, AliasManager $pathAliasManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->pathAliasManager = $pathAliasManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = ['onPathTranslation', -10];
    return $events;
  }

  /**
   * Adds custom headers to the response.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The kernel response event.
   */
  public function onPathTranslation(ResponseEvent $event) {
    $pathInfo = $event->getRequest()->getPathInfo();
    $pathname = Url::fromRoute('decoupled_router.path_translation')->toString();

    if ($pathInfo !== $pathname) {
      return;
    }

    $response = $event->getResponse();
    $output = Json::decode($response->getContent());
    if (!$output) {
      return;
    }

    // Skip routes that do not have an ENTITY TYPE or ID.
    if (!isset($output['entity']['type']) || !isset($output['entity']['id'])) {
      return;
    }

    $response->getCacheableMetadata()->addCacheContexts(['url.query_args:domain']);

    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $this->entityTypeManager
      ->getStorage($output['entity']['type'])
      ->load($output['entity']['id']);

    $settings = $this->getSettings($entity);

    $uuid = $event->getRequest()->query->get('domain');
    $current_domain = $this->getDomainByUuid($uuid);

    if ($current_domain) {
      $configs = $current_domain->getConfigs()->getConfiguration();
      $site_info = array_filter($configs, function ($setting) {
        return $setting['id'] === 'site_info';
      });
      if (!empty($site_info)) {
        $site_info = reset($site_info);
        $alias = $this->pathAliasManager->getAliasByPath($site_info['data']['site_frontpage']);
        $output['isHomePath'] = $alias === $entity->toUrl()->toString();
        $response->setData($output);
      }

    }

    // Skip if entity bundle doesn't support decoupled domain.
    if (!$settings || !$settings['status'] || $settings['router'] === 'display_page') {
      return;
    }

    $not_found = FALSE;
    $path = $event->getRequest()->query->get('path');
    $allowed_domains = $settings['allowed_domains'];
    $set_domains = array_map(function ($value) {
      return $value['target_id'];
    }, $entity->get('decoupled_domain')->getValue());

    if (!$current_domain) {
      $not_found = TRUE;
    }
    elseif (!in_array($current_domain->id(), $allowed_domains) && !empty($allowed_domains)) {
      $not_found = TRUE;
    }
    elseif (!in_array($current_domain->id(), $set_domains) && !empty($set_domains)) {
      $not_found = TRUE;
    }

    if ($not_found) {
      switch ($settings['router']) {
        case 'access_denied':
          $response->setData([
            'message' => $this->t(
              'Unable to resolve path @path.',
              ['@path' => $path]
            ),
            'details' => $this->t(
              'None of the available methods were able to find a match for this path.'
            ),
          ]);
          $response->setStatusCode(Response::HTTP_FORBIDDEN);
          break;

        case 'domain_redirect':
          if (!empty($set_domains)) {
            $domain = $this->getDomainById(reset($set_domains));
            $host = $domain->getDefaultDomain();
            $redirect_url = implode("", [
              $host['protocol'] . "://",
              $host['domain'],
              ($host['port'] != 80 ? ":" . $host['port'] : ""),
              $path,
            ]);
            $redirects_trace = $output['redirect'] ?? [];
            $redirects_trace[] = [
              'from' => $path,
              'to' => $redirect_url,
              'status' => Response::HTTP_FOUND,
            ];
            $response->setData(array_merge(
              $output,
              ['redirect' => $redirects_trace]
            ));
            break;
          }

        default:
          $response->setData([
            'message' => $this->t(
              'Unable to resolve path @path.',
              ['@path' => $path]
            ),
            'details' => $this->t(
              'None of the available methods were able to find a match for this path.'
            ),
          ]);
          $response->setStatusCode(Response::HTTP_NOT_FOUND);
          return;
      }
    }
  }

  /**
   * Retrieve domain by ID.
   *
   * @param string $id
   *   The ID of the domain to be loaded.
   *
   * @return \Drupal\decoupled_domain\Entity\DomainInterface|null
   *   The decoupled domain or NULL.
   */
  private function getDomainById($id) {
    return $this->entityTypeManager
      ->getStorage('decoupled_domain')
      ->load($id);
  }

  /**
   * Retrieve domain by UUID.
   *
   * @param string $uuid
   *   The UUID of the domain to be loaded.
   *
   * @return \Drupal\decoupled_domain\Entity\DomainInterface|null
   *   The decoupled domain or NULL.
   */
  private function getDomainByUuid($uuid) {
    $domains = $this->entityTypeManager
      ->getStorage('decoupled_domain')
      ->loadByProperties(['uuid' => $uuid]);
    return !empty($domains) ? reset($domains) : NULL;
  }

  /**
   * Get bundle settings of the entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity.
   *
   * @return array|null
   *   The decoupled domain settings for the current entity bundle.
   */
  private function getSettings(ContentEntityInterface $entity) {
    $entity_bundle_id = $entity->getEntityType()->getBundleEntityType();

    if ($entity_bundle_id) {
      $id = $entity_bundle_id . '_' . $entity->bundle();
    }
    else {
      $id = $entity->getEntityTypeId();
    }

    $settings = $this->entityTypeManager->getStorage('decoupled_domain_field_settings')->load($id);
    return isset($settings) ? $settings->getSettings() : NULL;
  }

}
