<?php

namespace Drupal\decoupled_domain_access;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Custom dynamic permissions.
 */
class EntityPermissions implements ContainerInjectionInterface {
  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static();
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * Returns an array of permissions.
   *
   * @return array
   *   The list with the permissions.
   *   @see \Drupal\user\PermissionHandlerInterface::getPermissions()
   */
  public function permissions() {
    $permissions = [];

    $instances = $this->entityTypeManager
      ->getStorage('decoupled_domain_field_settings')
      ->loadMultiple();

    foreach ($instances as $instance) {
      if (!$instance->isEnabled()) {
        continue;
      }
      $entity = NULL;

      $entity_type = $this->entityTypeManager->getDefinition($instance->getTargetEntityTypeId());
      if ($entity_id = $instance->getTargetEntityId()) {
        $entity = $this->entityTypeManager->getStorage($entity_type->id())->load($entity_id);
      }

      $label = $entity_type->getLabel() . (isset($entity) ? ": " . $entity->label() : "");

      $permissions['decoupled_domain.' . $instance->id()] = [
        'title' => $this->t('Bypass decoupled domain access for @entity_type', [
          '@entity_type' => $label,
        ]),
      ];
    }

    return $permissions;
  }

}
