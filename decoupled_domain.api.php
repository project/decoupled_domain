<?php

/**
 * @file
 * Hooks provided by the Decoupled Domain module.
 */

/**
 * Perform alterations to the decoupled domain settings before saved.
 *
 * @param string $setting
 *   The setting name.
 * @param mixed $value
 *   The value of the setting.
 * @param string $entity_type_id
 *   The entity type id.
 */
function hook_decoupled_domain_setting_pre_save(string $setting, &$value, string $entity_type_id) {
  if ($setting == 'allowed_domains' && is_array($value)) {
    $value = array_keys(array_filter($value));
  }
}
